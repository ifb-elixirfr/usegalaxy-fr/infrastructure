hostname: "{{ ansible_hostname }}"
nginx_remove_default_vhost: true
nginx_client_max_body_size: "20g"
nginx_server_names_hash_bucket_size: "128"
nginx_user: galaxy

nginx_upstreams:
  - name: galaxy_gunicorn
    servers:
      - "{{ groups.galaxy | first }}:{{ galaxy_gunicorn_port }}"
      - "{{ groups.galaxy | first }}:{{ galaxy_gunicorn_port +1 }}"

nginx_vhosts:
  - listen: "443"
    server_name: "~^www\\.(?<domain>.+)$"
    extra_parameters: |
      return 301 https://$domain$request_uri;
    filename: "www.all.443.conf"
  - listen: "80"
    server_name: "~^www\\.(?<domain>.+)$"
    extra_parameters: |
      return 301 https://$domain$request_uri;
    filename: "www.all.80.conf"
  - listen: "80"
    server_name: "{{ hostname }}"
    extra_parameters: |
      return 301 https://$host$request_uri;
    filename: "{{ hostname }}.80.conf"
  - listen: "443 ssl default_server"
    server_name: "~^(?<subdomain>[a-z0-9-]+).{{ galaxy_domain }}$ {{ hostname }}"
    access_log: "/var/log/nginx/access.log"
    error_log: "/var/log/nginx/error.log"
    state: "present"
    filename: "{{ hostname }}.443.conf"
    extra_parameters: |
      gzip on;
      gzip_http_version 1.1;
      gzip_vary on;
      gzip_comp_level 6;
      gzip_proxied any;
      gzip_types text/plain text/css application/javascript application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;
      gzip_buffers 16 8k;
      ssl_protocols TLSv1.2;
      ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA;
      ssl_prefer_server_ciphers on;
      ssl_ecdh_curve secp384r1:prime256v1;
      ssl_certificate "{{ galaxy_frontend_ssl_cert }}" ;
      ssl_certificate_key "{{ galaxy_frontend_ssl_key }}" ;
      client_max_body_size 10G; # aka max upload size, defaults to 1M
      uwsgi_read_timeout 2400;
      uwsgi_max_temp_file_size 0;
      proxy_read_timeout 2400;
      location /training-material/ {
          proxy_pass https://training.galaxyproject.org/training-material/;
      }

      ### bad gateway, galaxy is not reachable
      error_page 502 /maintenance.html;
      location = /maintenance.html {
        root /srv/maintenance;
        internal;
      }

      location / {
          proxy_pass http://galaxy_gunicorn;

          proxy_set_header Host $http_host;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto $scheme;
          proxy_set_header Upgrade $http_upgrade;

          # Maintenance mode
          #satisfy any;
          # Allow gitlab-ci to access
          #allow 192.54.201.184;
          #deny all;
          # Allow by passwd
          #auth_basic "Access restricted during maintenace";
          #auth_basic_user_file /etc/nginx/.htpasswd;

      }
      location /_x_accel_redirect/ {
        internal;
        alias /;
      }
      location /favicon.ico {
        return 301 https://ifb-elixirfr.gitlab.io/usegalaxy-fr/welcome/images/logo_usegalaxy-fr/usegalaxy-fr-favicon.png;
      }
      #location /static/welcome.html {
      #  alias /srv/welcome/$host/;
      #}
      #location ~ /static/style/base.css {
      #  root /srv/base-css;
      #  try_files /$host.css /{{ galaxy_domain }}.css =404;
      #}

      # Managing welcome.html
      location /static/welcome.html {

        # for subdomains
        set $welcome_file {{ subdomains_dir }}/$host.html; 

        # for labs
        if (-f {{ galaxy_server_dir }}/static-$subdomain/welcome.html/index.html) {
          set $welcome_file {{ galaxy_server_dir }}/static-$subdomain/welcome.html/index.html;
        }
        if (-f {{ galaxy_server_dir }}/static-$subdomain/welcome.html/welcome.html) {
          set $welcome_file {{ galaxy_server_dir }}/static-$subdomain/welcome.html/welcome.html;
        }
        alias $welcome_file;
      }

      # Managing static files
      location /static {

        # for subdomains
        set $static_dir {{ galaxy_server_dir }}/static;

        # for labs
        if (-d {{ galaxy_server_dir }}/static-$subdomain) {
          set $static_dir {{ galaxy_server_dir }}/static-$subdomain;
        }

        alias $static_dir;
        expires 24h;
      }

      location ~ /static/style/base.css {
          root {{ subdomains_dir }}/;
          try_files /$host.css /{{ galaxy_domain }}.css =404;
      }

      location /reports {
          proxy_pass http://{{ groups['galaxy'] | first }}:9002/;
          proxy_redirect        off;
          proxy_set_header      Host                 $http_host;
          proxy_set_header      X-Real-IP            $remote_addr;
          proxy_set_header      X-Forwarded-For      $proxy_add_x_forwarded_for;
          proxy_set_header      X-Forwarded-Proto $scheme;
          proxy_hide_header     X-Frame-Options;
          proxy_read_timeout    600;  # seconds
          auth_basic galaxy;
          auth_basic_user_file /etc/nginx/.reports_passwd;
      }

      location /flower/ {
          proxy_pass http://{{ groups['galaxy'] | first }}:5555/flower/;
          proxy_redirect        off;
          proxy_set_header      Host                 $http_host;
          proxy_set_header      X-Real-IP            $remote_addr;
          proxy_set_header      X-Forwarded-For      $proxy_add_x_forwarded_for;
          proxy_set_header      X-Forwarded-Proto $scheme;
          proxy_hide_header     X-Frame-Options;
          proxy_read_timeout    600;  # seconds
      }

      location /mqadmin/ {
          proxy_pass http://{{ groups['galaxy'] | first }}:15672/;
          proxy_redirect        off;
          proxy_set_header      Host                 $http_host;
          proxy_set_header      X-Real-IP            $remote_addr;
          proxy_set_header      X-Forwarded-For      $proxy_add_x_forwarded_for;
          proxy_set_header      X-Forwarded-Proto $scheme;
          proxy_hide_header     X-Frame-Options;
          proxy_read_timeout    600;  # seconds
      }

      {{ tiaas_nginx_routes }}

      location /api/upload/resumable_upload {
        # Disable request and response buffering
        proxy_request_buffering     off;
        proxy_buffering             off;
        proxy_http_version          1.1;

        # Add X-Forwarded-* headers
        proxy_set_header X-Forwarded-Host   $host;
        proxy_set_header X-Forwarded-Proto  $scheme;

        proxy_set_header Upgrade            $http_upgrade;
        proxy_set_header Connection         "upgrade";
        client_max_body_size        0;
        proxy_pass http://{{ groups.galaxy | first }}:{{ galaxy_tusd_port }}/files;
      }

      # Route all path-based interactive tool requests to the InteractiveTool proxy application
      location ~* ^/(interactivetool/.+)$ {
        proxy_redirect off;
        proxy_http_version 1.1;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_pass http://{{ groups.galaxy | first }}:{{ gie_proxy_port }};
        proxy_read_timeout 1d;
        proxy_buffering off;
      }

  - listen: "443 ssl"
    server_name: "*.ep.interactivetool.{{ galaxy_domain }} ~^(.*).ep.interactivetool.(.*).{{ galaxy_domain }}$"
    access_log: "/var/log/nginx/galaxy-gie-proxy-access.log"
    error_log: "/var/log/nginx/galaxy-gie-proxy-error.log"
    state: "present"
    filename: "{{ hostname }}.gie-proxy.conf"
    extra_parameters: |
      gzip on;
      gzip_http_version 1.1;
      gzip_vary on;
      gzip_comp_level 6;
      gzip_proxied any;
      gzip_types text/plain text/css text/xml text/javascript application/javascript application/x-javascript application/json application/xml application/xml+rss application/xhtml+xml application/x-font-ttf application/x-font-opentype image/png image/svg+xml image/x-icon;
      gzip_buffers 16 8k;
      ssl_protocols TLSv1.2;
      ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA;
      ssl_prefer_server_ciphers on;
      ssl_ecdh_curve secp384r1:prime256v1;
      ssl_certificate "{{ galaxy_frontend_ssl_cert }}" ;
      ssl_certificate_key "{{ galaxy_frontend_ssl_key }}" ;
      client_max_body_size 10G; # aka max upload size, defaults to 1M

      # Route all domain-based interactive tool requests to the InteractiveTool proxy application
      location / {
          proxy_redirect off;
          proxy_http_version 1.1;
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection "upgrade";
          proxy_pass http://{{ groups.galaxy | first }}:{{ gie_proxy_port }};
          proxy_read_timeout 1d;
          proxy_buffering off;
      }

  - listen: "443 ssl"
    server_name: "{{ sentry_domain }}"
    access_log: "/var/log/nginx/galaxy-sentry-access.log"
    error_log: "/var/log/nginx/galaxy-sentry-error.log"
    state: "present"
    filename: "{{ hostname }}.sentry.conf"
    extra_parameters: |
      gzip on;
      gzip_http_version 1.1;
      gzip_vary on;
      gzip_comp_level 6;
      gzip_proxied any;
      gzip_types text/plain text/css text/xml text/javascript application/javascript application/x-javascript application/json application/xml application/xml+rss application/xhtml+xml application/x-font-ttf application/x-font-opentype image/png image/svg+xml image/x-icon;
      gzip_buffers 16 8k;
      ssl_protocols TLSv1.2;
      ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA;
      ssl_prefer_server_ciphers on;
      ssl_ecdh_curve secp384r1:prime256v1;
      ssl_certificate "{{ galaxy_frontend_ssl_cert }}" ;
      ssl_certificate_key "{{ galaxy_frontend_ssl_key }}" ;
      client_max_body_size 10G; # aka max upload size, defaults to 1M
      location / {
          proxy_redirect off;
          proxy_http_version 1.1;
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection "upgrade";
          proxy_pass http://{{ (groups['grafana'] | map('extract', hostvars) | map(attribute='ansible_host'))[0] }}:9000;
          proxy_read_timeout 1d;
          proxy_buffering off;
      }
