$(document).ready(function() {

    var citationNeededView = Backbone.View.extend({
        el: '#citation_needed',

        appTemplate: _.template(
            '<div id="cit_need-header">' +
                '<h3>We need your support ...</h3>' +
            '</div>' +
            '<div id="cit_need-body">' +
            'If Galaxy helped with the analysis of your data, please do not forget to <b>cite</b>:' +
            '<div id="cit_need-citation">'+
            'The Galaxy platform for accessible, reproducible, and collaborative data analyses: 2024 update<br>'+
            'Nucleic Acids Research, gkae410<br>'+
            'doi:10.1093/nar/gkae410'+
            '</div>' +
            'And please <b>acknowledge</b> the French Galaxy server:' +
            '<div id="cit_need-citation">'+
            'The Galaxy server used for some calculations is funded by the IFB-core, Institut Français de Bioinformatique (IFB), CNRS, INSERM, INRAE, CEA, 91057 Evry, France ' +            '</div>' +
            '</div>'
        ),


        initialize: function() {
            var me = this;
            this.render();
        },

        render: function() {
            this.$el.html(this.appTemplate());
            return this;
        }

    });

    var citNeedApp = new citationNeededView;

});