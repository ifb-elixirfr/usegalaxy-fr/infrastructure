$(document).ready(function() {
    $.get("/api/whoami", function(data) {
        if (data === null || (typeof data === 'object' && data.id === '0')) {
            var advertiseAnonUsersView = Backbone.View.extend({
                el: '#advertise_anon_users',
                appTemplate: _.template(
                    '<div id="adver_anon-header">' +
                    '<h3>Welcome to usegalaxy.fr!</h3>' +
                    '</div>' +
                    '<div id="adver_anon-body">' +
                    'To take full advantage of all the features offered by our platform, we encourage you to log in or create an account now. By registering, you ll be able to save your analyses, synchronize your data between different devices and access exclusive services.' +
                    '<div id="adver_anon-link">'+
                    '<p><a href="/login/start?redirect=None">Log in or Register a new account!</a></p>' +
                    '</div>' +
                    'Creating an account is quick and easy, and will help you optimize your user experience.' +
                    '</div>'
                ),
                initialize: function() {
                    this.render();
                },
                render: function() {
                    this.$el.html(this.appTemplate());
                    return this;
                }
            });
            var adverAnonApp = new advertiseAnonUsersView();
        }
    });
});