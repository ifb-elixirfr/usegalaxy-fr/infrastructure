#!/bin/bash

source $1   #conda.sh
conda create --yes --override-channels --channel conda-forge --channel defaults --name _galaxy_ "python=3.11.6" "pip>=9" "virtualenv>=16"
conda activate _galaxy_
virtualenv $2   #galaxy_source_dir/.venv
chown -R $3 $2
